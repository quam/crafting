import { Component, HostListener } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  title = 'Crafting';

  @HostListener('document:keydown.h')
  toggle_visible()
  {
    var hidden = localStorage.getItem('ShowHidden') == 'true'
    if(hidden) {
      localStorage.setItem('ShowHidden', 'false');
    }
    else {
      localStorage.setItem('ShowHidden', 'true');
    }
  }
}
