import { Component, OnInit } from '@angular/core';
import { RouterServiceService } from 'src/app/common/router-service.service';

@Component({
  selector: 'app-pigments',
  templateUrl: './pigments.component.html',
  styleUrls: ['./pigments.component.scss']
})
export class PigmentsComponent implements OnInit {

  constructor(private router: RouterServiceService) {
    router.navigate();
  }

  ngOnInit(): void {
  }

}
