import { Component, OnInit } from '@angular/core';
import { RouterServiceService } from 'src/app/common/router-service.service';

@Component({
  selector: 'app-tinctures',
  templateUrl: './tinctures.component.html',
  styleUrls: ['./tinctures.component.scss']
})
export class TincturesComponent implements OnInit {

  constructor(private router: RouterServiceService) {
    router.navigate();
  }

  ngOnInit(): void {
  }

}
