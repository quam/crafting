import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TincturesComponent } from './tinctures.component';

describe('TincturesComponent', () => {
  let component: TincturesComponent;
  let fixture: ComponentFixture<TincturesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TincturesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TincturesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
