import { Component, OnInit } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { RouterServiceService } from 'src/app/common/router-service.service';

@Component({
  selector: 'app-explosives',
  templateUrl: './explosives.component.html',
  styleUrls: ['./explosives.component.scss'],
})
export class ExplosivesComponent implements OnInit {
  constructor(private router: RouterServiceService) {
    router.navigate();
  }

  ngOnInit(): void {}
}
