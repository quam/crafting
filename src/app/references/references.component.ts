import { Component, OnInit } from '@angular/core';
import { RouterServiceService } from '../common/router-service.service';

@Component({
  selector: 'app-references',
  templateUrl: './references.component.html',
  styleUrls: ['./references.component.scss']
})
export class ReferencesComponent implements OnInit {

  references: any = [
    {
      title: 'Potions',
      text: 'Recipes for magical potions and poisons.',
      link: "/references/potions",
    },
    {
      title: 'Pigments',
      text: 'Different pigments and inks and their recipes.',
      link: "/references/pigments",
    },
    {
      title: 'Tinctures',
      text: 'Oils, perfumes and other chemicals.',
      link: "/references/tinctures",
    },
    {
      title: 'Explosives',
      text: 'Fire, smoke and explosions.',
      link: "/references/explosives",
    },
  ];

  constructor(private router: RouterServiceService) {
    router.navigate();
  }

  ngOnInit(): void {
  }

}
