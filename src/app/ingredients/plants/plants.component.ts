import { Component, OnInit } from '@angular/core';
import { IngredientsService } from 'src/app/ingredients/ingredients.service';
import { RouterServiceService } from 'src/app/common/router-service.service';

@Component({
  selector: 'app-plants',
  templateUrl: './plants.component.html',
  styleUrls: ['./plants.component.scss']
})
export class PlantsComponent implements OnInit {

  plants: any;

  constructor(private router: RouterServiceService, private ingredients: IngredientsService) {
    router.navigate();
    this.plants = ingredients.plants;
  }

  ngOnInit(): void {
  }

}
