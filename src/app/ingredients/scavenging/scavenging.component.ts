import { Component, OnInit } from '@angular/core';
import { RouterServiceService } from 'src/app/common/router-service.service';

@Component({
  selector: 'app-scavenging',
  templateUrl: './scavenging.component.html',
  styleUrls: ['./scavenging.component.scss']
})
export class ScavengingComponent implements OnInit {

  constructor(private router: RouterServiceService) {
    router.navigate();
  }

  ngOnInit(): void {
  }

}
