import { Component, OnInit } from '@angular/core';
import { IngredientsService } from 'src/app/ingredients/ingredients.service';
import { RouterServiceService } from 'src/app/common/router-service.service';

@Component({
  selector: 'app-resources',
  templateUrl: './resources.component.html',
  styleUrls: ['./resources.component.scss']
})
export class ResourcesComponent implements OnInit {

  resources: any;

  constructor(private router: RouterServiceService, private ingredients: IngredientsService) {
    router.navigate();
    this.resources = ingredients.resources;
  }

  ngOnInit(): void {
  }

}
