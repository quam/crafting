import { Component, OnInit } from '@angular/core';
import { RouterServiceService } from '../common/router-service.service';

@Component({
  selector: 'app-ingredients',
  templateUrl: './ingredients.component.html',
  styleUrls: ['./ingredients.component.scss']
})
export class IngredientsComponent implements OnInit {

  constructor(private router: RouterServiceService) {
    router.navigate();
  }

  ngOnInit(): void {
  }

  ingredients: any = [
    {
      title: 'Resources',
      text: 'From healing mud to toxic mercury ore world is full of useful resources.',
      link: "/ingredients/resources",
    },
    {
      title: 'Plants',
      text: 'List of known plants of the Southern Continent and the plains beyond.',
      link: "/ingredients/plants",
    },
    {
      title: 'Animals',
      text: 'Different animals that can be used and harvested for their magic.',
      link: "/ingredients/animals",
    },
    {
      title: 'Scavenging',
      text: 'Gathering food, water and wood in the different biomes across the plains.',
      link: "/ingredients/scavenging",
    },
  ];

}
