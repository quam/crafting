import { Component, OnInit } from '@angular/core';
import { Ingredient } from 'src/app/ingredients/ingredients.service';
import { IngredientsService } from 'src/app/ingredients/ingredients.service';
import { RouterServiceService } from 'src/app/common/router-service.service';

@Component({
  selector: 'app-animals',
  templateUrl: './animals.component.html',
  styleUrls: ['./animals.component.scss']
})
export class AnimalsComponent implements OnInit {

  animals: Array<Ingredient>;

  constructor(private router: RouterServiceService, private ingredients: IngredientsService) {
    router.navigate();
    this.animals = ingredients.animals;
  }

  ngOnInit(): void {
  }

}
