import { Component, Input, OnInit } from '@angular/core';
import { Ingredient } from 'src/app/ingredients/ingredients.service';

@Component({
  selector: 'app-ingredient',
  templateUrl: './ingredient.component.html',
  styleUrls: ['./ingredient.component.scss']
})
export class IngredientComponent implements OnInit {

  @Input()data: Ingredient

  constructor() {
    this.data = {
      title: '',
      description: ``,
      biomes: [],
      comment: '',
      c: 0,
      a: 0,
      p: 0,
      g: 0,
      u: 0,
    }
  }

  ngOnInit(): void {
  }

}
