import { Component, Input, OnInit } from '@angular/core';
import { Ingredient } from 'src/app/ingredients/ingredients.service';

@Component({
  selector: 'app-alphabetical-list',
  templateUrl: './alphabetical-list.component.html',
  styleUrls: ['./alphabetical-list.component.scss']
})


export class AlphabeticalListComponent implements OnInit {

  @Input()data: Array<Ingredient>

  filtered_data: Array<Ingredient>;

  

  set_data(letter: string) {
    this.filtered_data = [];
    for(var i=0; i<this.data.length; ++i) {
      if(this.data[i].title.toLowerCase().startsWith(letter)) {
        this.filtered_data.push(this.data[i]);
      }
    }
  }

  constructor() {
    this.filtered_data = [];
    this.data = [];
  }

  ngOnInit(): void {
    this.set_data('a');
  }

}
