import { Component, OnInit } from '@angular/core';
import { RouterServiceService } from '../common/router-service.service';

@Component({
  selector: 'app-introduction',
  templateUrl: './introduction.component.html',
  styleUrls: ['./introduction.component.scss']
})
export class IntroductionComponent implements OnInit {

  constructor(private router: RouterServiceService) {
    router.navigate();
  }

  ngOnInit(): void {
  }

  introduction: any = [
    {
      title: 'Introduction for DMs',
      text: 'Design choices, notes and ideas for using this guide in your own games.',
      link: '/introduction/intro-for-dms',
    },
    {
      title: 'Introduction for Players',
      text: 'Things you need to know as a player to use this system effectively.',
      link: '/introduction/intro-for-players',
    },
    {
      title: 'Alchemical Components',
      text: 'Explanation of Alchemical Components used as the base of the alchemical system of crafts and magic.',
      link: '/introduction/components',
    },
  ];

}
