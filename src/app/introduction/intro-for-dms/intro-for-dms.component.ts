import { Component, OnInit } from '@angular/core';
import { RouterServiceService } from 'src/app/common/router-service.service';

@Component({
  selector: 'app-intro-for-dms',
  templateUrl: './intro-for-dms.component.html',
  styleUrls: ['./intro-for-dms.component.scss']
})
export class IntroForDmsComponent implements OnInit {

  constructor(private router: RouterServiceService) {
    router.navigate();
  }

  ngOnInit(): void {
  }

}
