import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IntroForDmsComponent } from './intro-for-dms.component';

describe('IntroForDmsComponent', () => {
  let component: IntroForDmsComponent;
  let fixture: ComponentFixture<IntroForDmsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IntroForDmsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IntroForDmsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
