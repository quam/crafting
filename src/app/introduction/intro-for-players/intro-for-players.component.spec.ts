import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IntroForPlayersComponent } from './intro-for-players.component';

describe('IntroForPlayersComponent', () => {
  let component: IntroForPlayersComponent;
  let fixture: ComponentFixture<IntroForPlayersComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IntroForPlayersComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IntroForPlayersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
