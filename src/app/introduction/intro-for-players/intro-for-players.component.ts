import { Component, OnInit } from '@angular/core';
import { RouterServiceService } from 'src/app/common/router-service.service';

@Component({
  selector: 'app-intro-for-players',
  templateUrl: './intro-for-players.component.html',
  styleUrls: ['./intro-for-players.component.scss']
})
export class IntroForPlayersComponent implements OnInit {

  constructor(private router: RouterServiceService) {
    router.navigate();
  }

  ngOnInit(): void {
  }

}
