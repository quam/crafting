import { Component, OnInit } from '@angular/core';
import { RouterServiceService } from '../common/router-service.service';

@Component({
  selector: 'app-main-menu',
  templateUrl: './main-menu.component.html',
  styleUrls: ['./main-menu.component.scss'],
})
export class MainMenuComponent implements OnInit {

  main: any = [
    {
      title: 'Introduction',
      text: 'Introduction to this system of crafting.',
      link: "introduction",
    },
    {
      title: 'Tools',
      text: 'Complete list of tools with crafting recipes for each.',
      link: "tools",
    },
    {
      title: 'References',
      text: 'Things that can be created using multiple tools.',
      link: "references",
    },
    {
      title: 'Ingredients',
      text: 'Full list of ingredients used for crafting.',
      link: "ingredients",
    },
  ];

  constructor(private router: RouterServiceService) {
    router.navigate();
  }

  ngOnInit(): void {}
}
