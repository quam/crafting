import { NgModule } from '@angular/core';
import { ExtraOptions, RouterModule, Routes } from '@angular/router';

//=================================MAIN MENU==================================//
import { MainMenuComponent } from './main-menu/main-menu.component';
//================================INTRODUCTION================================//
import { IntroductionComponent } from './introduction/introduction.component';
import { ComponentsComponent } from './introduction/components/components.component';
import { IntroForDmsComponent } from './introduction/intro-for-dms/intro-for-dms.component';
import { IntroForPlayersComponent } from './introduction/intro-for-players/intro-for-players.component';
//====================================TOOLS===================================//
import { ToolsComponent } from './tools/tools.component';
import { AlchemistComponent } from './tools/alchemist/alchemist.component';
import { BrewerComponent } from './tools/brewer/brewer.component';
import { CalligrapherComponent } from './tools/calligrapher/calligrapher.component';
import { CarpenterComponent } from './tools/carpenter/carpenter.component';
import { CartographerComponent } from './tools/cartographer/cartographer.component';
import { CobblerComponent } from './tools/cobbler/cobbler.component';
import { CookComponent } from './tools/cook/cook.component';
import { DisguiseComponent } from './tools/disguise/disguise.component';
import { ForgeryComponent } from './tools/forgery/forgery.component';
import { GlassblowerComponent } from './tools/glassblower/glassblower.component';
import { HealerComponent } from './tools/healer/healer.component';
import { HerbalismComponent } from './tools/herbalism/herbalism.component';
import { JewelerComponent } from './tools/jeweler/jeweler.component';
import { LeatherworkerComponent } from './tools/leatherworker/leatherworker.component';
import { MasonComponent } from './tools/mason/mason.component';
import { NavigatorComponent } from './tools/navigator/navigator.component';
import { PainterComponent } from './tools/painter/painter.component';
import { PoisonerComponent } from './tools/poisoner/poisoner.component';
import { PotterComponent } from './tools/potter/potter.component';
import { SmithComponent } from './tools/smith/smith.component';
import { ThievesComponent } from './tools/thieves/thieves.component';
import { TinkerComponent } from './tools/tinker/tinker.component';
import { WeaverComponent } from './tools/weaver/weaver.component';
import { WoodcarverComponent } from './tools/woodcarver/woodcarver.component';
//================================INGREDIENTS=================================//
import { IngredientsComponent } from './ingredients/ingredients.component';
import { AnimalsComponent } from './ingredients/animals/animals.component';
import { PlantsComponent } from './ingredients/plants/plants.component';
import { ResourcesComponent } from './ingredients/resources/resources.component';
import { ScavengingComponent } from './ingredients/scavenging/scavenging.component';
//=================================REFERENCES=================================//
import { ReferencesComponent } from './references/references.component';
import { ExplosivesComponent } from './references/explosives/explosives.component';
import { PigmentsComponent } from './references/pigments/pigments.component';
import { PotionsComponent } from './references/potions/potions.component';
import { TincturesComponent } from './references/tinctures/tinctures.component';

const extraOptions: ExtraOptions = {
  scrollPositionRestoration: 'enabled',
  anchorScrolling: 'enabled',
  scrollOffset: [0, 64],
  onSameUrlNavigation: 'reload',
  useHash: true,
};

const routes: Routes = [
//=================================MAIN MENU==================================//
  {
    path: '',
    component: MainMenuComponent,
  },
//================================INTRODUCTION================================//
  {
    path: 'introduction',
    component: IntroductionComponent,
  },
  {
    path: 'introduction/components',
    component: ComponentsComponent,
  },
  {
    path: 'introduction/intro-for-dms',
    component: IntroForDmsComponent,
  },
  {
    path: 'introduction/intro-for-players',
    component: IntroForPlayersComponent,
  },
//====================================TOOLS===================================//
  {
    path: 'tools',
    component: ToolsComponent,
  },
  {
    path: 'tools/alchemist',
    component: AlchemistComponent,
  },
  {
    path: 'tools/brewer',
    component: BrewerComponent,
  },
  {
    path: 'tools/calligrapher',
    component: CalligrapherComponent,
  },
  {
    path: 'tools/carpenter',
    component: CarpenterComponent,
  },
  {
    path: 'tools/cartographer',
    component: CartographerComponent,
  },
  {
    path: 'tools/cobbler',
    component: CobblerComponent,
  },
  {
    path: 'tools/cook',
    component: CookComponent,
  },
  {
    path: 'tools/disguise',
    component:DisguiseComponent,
  },
  {
    path: 'tools/forgery',
    component: ForgeryComponent,
  },
  {
    path: 'tools/glassblower',
    component: GlassblowerComponent,
  },
  {
    path: 'tools/healer',
    component: HealerComponent,
  },
  {
    path: 'tools/herbalism',
    component: HerbalismComponent,
  },
  {
    path: 'tools/jeweler',
    component: JewelerComponent,
  },
  {
    path: 'tools/leatherworker',
    component: LeatherworkerComponent,
  },
  {
    path: 'tools/mason',
    component: MasonComponent,
  },
  {
    path: 'tools/navigator',
    component: NavigatorComponent,
  },
  {
    path: 'tools/painter',
    component: PainterComponent,
  },
  {
    path: 'tools/poisoner',
    component: PoisonerComponent,
  },
  {
    path: 'tools/potter',
    component: PotterComponent,
  },
  {
    path: 'tools/smith',
    component: SmithComponent,
  },
  {
    path: 'tools/thieves',
    component: ThievesComponent,
  },
  {
    path: 'tools/tinker',
    component: TinkerComponent,
  },
  {
    path: 'tools/weaver',
    component: WeaverComponent,
  },
  {
    path: 'tools/woodcarver',
    component: WoodcarverComponent,
  },
//================================INGREDIENTS=================================//
  {
    path: 'ingredients',
    component: IngredientsComponent,
  },
  {
    path: 'ingredients/animals',
    component: AnimalsComponent,
  },
  {
    path: 'ingredients/plants',
    component: PlantsComponent,
  },
  {
    path: 'ingredients/resources',
    component: ResourcesComponent,
  },
  {
    path: 'ingredients/scavenging',
    component: ScavengingComponent,
  },
//=================================REFERENCES=================================//
  {
    path: 'references',
    component: ReferencesComponent,
  },
  {
    path: 'references/explosives',
    component: ExplosivesComponent,
  },
  {
    path: 'references/pigments',
    component: PigmentsComponent,
  },
  {
    path: 'references/potions',
    component: PotionsComponent,
  },
  {
    path: 'references/tinctures',
    component: TincturesComponent,
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, extraOptions)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
