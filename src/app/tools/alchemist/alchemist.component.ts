import { Component, OnInit } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { RouterServiceService } from 'src/app/common/router-service.service';

@Component({
  selector: 'app-alchemist',
  templateUrl: './alchemist.component.html',
  styleUrls: ['./alchemist.component.scss']
})
export class AlchemistComponent implements OnInit {

  constructor(private router: RouterServiceService) {
    router.navigate();
  }

  ngOnInit(): void {
  }

  minor_panacea: any = {
    name: "Minor Panacea",
    type: "potion",
    rarity: "uncommon",
    text: `Allows creature that is sprinkled with this translucent liquid to
      immediately take another saving throw against a condition affecting it, if
      there is any. Creature can also immediately attempt a DC 10 constitution
      check to remove one level of exhaustion. Creature regains 1d4 Hit points.
      Creature stops aging for 1 year.`
  }

  minor_philosopher: any = {
    name: "Minor Philosopher's Stone",
    type: "wondrous item",
    rarity: "rare",
    text: `Stone translucent white stone holds 3 (1d4+1) charges. When all of the
    charges are consumed the stone is destroyed. You can use one of its charges
    to do one of the following:
    <ul class="list">
      <li>Create minor panacea out of regular water</li>
      <li>
        Transform 10 copper pieces worth of copper into 1 silver piece worth of
        silver or vice versa
      </li>
      <li>
        Transform 10 silver pieces worth of silver into 1 gold piece worth of gold
        or vice versa
      </li>
      <li>
        Transform 10 gold pieces worth of gold into 1 platinum piece worth of
        platinum or vice versa
      </li>
    </ul>`
  }

  panacea: any = {
    name: "Panacea",
    type: "potion",
    rarity: "rare",
    text: `Allows creature that is sprinkled with this translucent liquid to
    immediately end one condition affecting it, if it allows for saving throw.
    Creature also loses one level of exhaustion. Creature regains 2d4 Hit
    points. Creature gets 1 year younger down to its adult age and stops aging
    for 10 years.`
  }

  philosopher: any = {
    name: "Philosopher's Stone",
    type: "wondrous item",
    rarity: "rare",
    text: `This translucent blue stone holds 6
    (2d4+1) charges. When all of the charges are consumed the stone is
    destroyed. You can use one of its charges to do one of the following:
    <ul>
      <li>Create panacea out of regular water</li>
      <li>
        Transform 10 copper pieces worth of copper into 2 silver piece worth of
        silver
      </li>
      <li>
        Transform 1 silver piece worth of silver into 20 copper pieces worth of
        copper
      </li>
      <li>
        Transform 10 silver pieces worth of silver into 2 gold piece worth of gold
      </li>
      <li>
        Transform 1 gold piece worth of gold into 20 silver pieces worth of silver
      </li>
      <li>
        Transform 10 gold pieces worth of gold into 2 platinum pieces worth of
        platinum
      </li>
      <li>
        Transform 1 platinum piece worth of platinum into 20 gold pieces worth of
        gold
      </li>
      <li>10 gold worth of one kind of gemstone into 10 gold worth of another</li>
    </ul>`
  }

  major_panacea: any = {
    name: "Major Panacea",
    type: "potion",
    rarity: "very rare",
    text: `Allows creature that is sprinkled with this translucent liquid to
    immediately end all conditions affecting it, if they allows for saving
    throw. Creature also loses up to two levels of exhaustion. Creature regains
    half of its hit points, rounded down. Creature gets 2 year younger down to
    its adult age and stops aging for 20 years.`
  }

  major_philosopher: any = {
    name: "Major Philosopher's Stone",
    type: "wondrous item",
    rarity: "legendary",
    text: `This electric blue dimly glowing stone holds 8
    (3d4+1) charges. When all of the charges are consumed the stone is
    destroyed. You can use one of its charges to do one of the following:
    <ul class="list">
      <li>Create major panacea out of regular water</li>
      <li>
        Transform 10 copper pieces worth of copper into 5 silver piece worth of
        silver
      </li>
      <li>
        Transform 1 silver piece worth of silver into 50 copper pieces worth of
        copper
      </li>
      <li>
        Transform 10 silver pieces worth of silver into 5 gold piece worth of gold
      </li>
      <li>
        Transform 1 gold piece worth of gold into 50 silver pieces worth of silver
      </li>
      <li>
        Transform 10 gold pieces worth of gold into 5 platinum pieces worth of
        platinum
      </li>
      <li>
        Transform 1 platinum piece worth of platinum into 50 gold pieces worth of
        gold
      </li>
      <li>50 gold worth of one kind of gemstone into 50 gold worth of another</li>
    </ul>`
  }

  perfect_panacea: any = {
    name: "Perfect Panacea",
    type: "potion",
    rarity: "legendary",
    text: `Allows creature that is sprinkled with this translucent liquid to
    immediately end all conditions affecting it. Creature also loses all levels
    of exhaustion. Creature regains all of its hit points. Creature gets 10
    years younger down to its adult age and stops aging for 100 years.`
  }

  perfect_philosopher: any = {
    name: "Perfect Philosopher's Stone",
    type: "wondrous item",
    rarity: "legendary",
    text: `This completely translucent yet brightly glowing stone holds 11
    (4d4+1) charges. When all of the charges are consumed the stone is
    destroyed. You can use one of its charges to do one of the following:
    <ul class="list">
      <li>Create perfect panacea out of regular water</li>
      <li>
        Transform 10 copper pieces worth of copper into 10 silver piece worth of
        silver
      </li>
      <li>
        Transform 1 silver piece worth of silver into 100 copper pieces worth of
        copper
      </li>
      <li>
        Transform 10 silver pieces worth of silver into 10 gold piece worth of
        gold
      </li>
      <li>
        Transform 1 gold piece worth of gold into 100 silver pieces worth of
        silver
      </li>
      <li>
        Transform 10 gold pieces worth of gold into 10 platinum pieces worth of
        platinum
      </li>
      <li>
        Transform 1 platinum piece worth of platinum into 100 gold pieces worth of
        gold
      </li>
      <li>
        100 gold worth of one kind of gemstone into 100 gold worth of another
      </li>
    </ul>`
  }

  tiny_humunculus: any = {
    name: "Tiny Homunculus",
    size: "Tiny",
    type: "Humanoid (Homunculus)",
    ac: "12 (natural armor)",
    hp: "4 (1d4 +2)",
    speed: "20ft.",
    str: "4 (-3)",
    dex: "10 (+0)",
    con: "14 (+2)",
    int: "6 (-2)",
    wis: "8 (-1)",
    cha: "3 (-4)",
    sense: "Passive Perception 9",
    languages: "Limited telepathy",
    challenge: "0 (0 xp)",
    abilities: [
      {
        name: "Upgradeability",
        text: `Tiny homunculus can have one upgrade active at the
          time.`,
      },
      {
        name: "Repair",
        text: `It takes 0.5 hours and 10 gp worth of metals to repair 1 HP
          of damage`,
      },
    ],
  };

  small_humunculus: any = {
    name: "Small Homunculus",
    size: "Small",
    type: "Humanoid (Homunculus)",
    ac: "13 (natural armor)",
    hp: "9 (2d6 +2)",
    speed: "20ft.",
    str: "6 (-2)",
    dex: "12 (+1)",
    con: "14 (+2)",
    int: "6 (-2)",
    wis: "10 (+0)",
    cha: "3 (-4)",
    sense: "Passive Perception 10",
    languages: "Limited telepathy",
    challenge: "0 (0 xp)",
    abilities: [
      {
        name: "Upgradeability",
        text: `Small homunculus can have two upgrades active at the
          time.`,
      },
      {
        name: "Repair",
        text: `It takes 0.5 hours and 10 gp worth of metals to repair 1 HP
          of damage`,
      },
    ],
  };



  true_humunculus: any = {
    name: "True Homunculus",
    size: "Medium",
    type: "Humanoid (Homunculus)",
    ac: "14 (natural armor)",
    hp: "15 (3d8 +2)",
    speed: "20ft.",
    str: "8 (-1)",
    dex: "12 (+1)",
    con: "14 (+2)",
    int: "6 (-2)",
    wis: "12 (+1)",
    cha: "3 (-4)",
    sense: "Passive Perception 11",
    languages: "Limited telepathy",
    challenge: "0 (0 xp)",
    abilities: [
      {
        name: "Upgradeability",
        text: `Real homunculus can have four upgrades active at the
          time.`,
      },
      {
        name: "Repair",
        text: `It takes 0.5 hours and 10 gp worth of metals to repair 1 HP
          of damage`,
      },
    ],
  };
}
