import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GlassblowerComponent } from './glassblower.component';

describe('GlassblowerComponent', () => {
  let component: GlassblowerComponent;
  let fixture: ComponentFixture<GlassblowerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GlassblowerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GlassblowerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
