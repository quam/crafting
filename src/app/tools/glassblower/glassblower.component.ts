import { Component, OnInit } from '@angular/core';
import { RouterServiceService } from 'src/app/common/router-service.service';

@Component({
  selector: 'app-glassblower',
  templateUrl: './glassblower.component.html',
  styleUrls: ['./glassblower.component.scss']
})
export class GlassblowerComponent implements OnInit {

  constructor(private router: RouterServiceService) {
    router.navigate();
  }

  ngOnInit(): void {
  }

}
