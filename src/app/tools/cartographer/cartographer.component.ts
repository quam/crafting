import { Component, OnInit } from '@angular/core';
import { RouterServiceService } from 'src/app/common/router-service.service';

@Component({
  selector: 'app-cartographer',
  templateUrl: './cartographer.component.html',
  styleUrls: ['./cartographer.component.scss']
})
export class CartographerComponent implements OnInit {

  constructor(private router: RouterServiceService) {
    router.navigate();
  }

  ngOnInit(): void {
  }

}
