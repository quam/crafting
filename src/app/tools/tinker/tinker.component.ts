import { Component, OnInit } from '@angular/core';
import { RouterServiceService } from 'src/app/common/router-service.service';

@Component({
  selector: 'app-tinker',
  templateUrl: './tinker.component.html',
  styleUrls: ['./tinker.component.scss']
})
export class TinkerComponent implements OnInit {

  constructor(private router: RouterServiceService) {
    router.navigate();
  }

  ngOnInit(): void {
  }

}
