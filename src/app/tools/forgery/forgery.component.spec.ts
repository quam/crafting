import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ForgeryComponent } from './forgery.component';

describe('ForgeryComponent', () => {
  let component: ForgeryComponent;
  let fixture: ComponentFixture<ForgeryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ForgeryComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ForgeryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
