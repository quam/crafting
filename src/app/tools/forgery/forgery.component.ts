import { Component, OnInit } from '@angular/core';
import { RouterServiceService } from 'src/app/common/router-service.service';

@Component({
  selector: 'app-forgery',
  templateUrl: './forgery.component.html',
  styleUrls: ['./forgery.component.scss']
})
export class ForgeryComponent implements OnInit {

  constructor(private router: RouterServiceService) {
    router.navigate();
  }

  ngOnInit(): void {
  }

}
