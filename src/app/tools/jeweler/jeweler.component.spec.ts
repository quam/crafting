import { ComponentFixture, TestBed } from '@angular/core/testing';

import { JewelerComponent } from './jeweler.component';

describe('JewelerComponent', () => {
  let component: JewelerComponent;
  let fixture: ComponentFixture<JewelerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ JewelerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(JewelerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
