import { Component, OnInit } from '@angular/core';
import { RouterServiceService } from 'src/app/common/router-service.service';

@Component({
  selector: 'app-jeweler',
  templateUrl: './jeweler.component.html',
  styleUrls: ['./jeweler.component.scss']
})
export class JewelerComponent implements OnInit {

  constructor(private router: RouterServiceService) {
    router.navigate();
  }

  ngOnInit(): void {
  }

}
