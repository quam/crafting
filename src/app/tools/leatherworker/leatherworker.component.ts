import { Component, OnInit } from '@angular/core';
import { RouterServiceService } from 'src/app/common/router-service.service';

@Component({
  selector: 'app-leatherworker',
  templateUrl: './leatherworker.component.html',
  styleUrls: ['./leatherworker.component.scss']
})
export class LeatherworkerComponent implements OnInit {

  constructor(private router: RouterServiceService) {
    router.navigate();
  }

  ngOnInit(): void {
  }

}
