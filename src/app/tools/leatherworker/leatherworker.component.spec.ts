import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LeatherworkerComponent } from './leatherworker.component';

describe('LeatherworkerComponent', () => {
  let component: LeatherworkerComponent;
  let fixture: ComponentFixture<LeatherworkerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LeatherworkerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LeatherworkerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
