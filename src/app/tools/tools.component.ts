import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-tools',
  templateUrl: './tools.component.html',
  styleUrls: ['./tools.component.scss'],
})
export class ToolsComponent implements OnInit {

  tools: any = [
    {
      title: "Alchemist's Supplies",
      text: 'Explorers of reality, high magic and life itself, alchemists look to transform the world in their image.',
      link: '/tools/alchemist',
    },
    {
      title: "Brewer's Supplies",
      text: 'Shared drink takes relationships to the next level with humans and gods alike, brewers hone their craft with extreme dedication.',
      link: '/tools/brewer',
    },
    {
      title: "Calligrapher's Supplies",
      text: 'From exploding text to the love letter, calligraphers understand the power of a written word.',
      link: '/tools/calligrapher',
    },
    {
      title: "Carpenter's Tools",
      text: 'From chest with extra storage to the fixing of a sinking ship adventuring carpenters are welcome in any party.',
      link: '/tools/carpenter',
    },
    {
      title: "Cartographer's Tools",
      text: "Retracing steps, reading maps and finding new routes are all in an adventuring cartographer's tool set.",
      link: '/tools/cartographer',
    },
    {
      title: "Cobbler's Tools",
      text: 'While sometimes considered a humble profession, every adventurer learns how to appreciate a good pair of boots.',
      link: '/tools/cobbler',
    },
    {
      title: "Cook's Utensil",
      text: 'Creating delicious meal out of sparse ingredients, good cooks can feed their party even when others would have to go hungry.',
      link: "/tools/cook",
    },
    {
      title: 'Disguise Kit',
      text: 'While being treasured as performers, masters of disguise can become anyone at any time.',
      link: "/tools/disguise",
    },
    {
      title: 'Forgery Kit',
      text: 'Master forgers always have the right papers for the occasion and their art might as well be the original.',
      link: "/tools/forgery",
    },
    {
      title: "Glassblower's Tools",
      text: 'Glassblowers turn sand into delicate translucent creations useful in many different areas.',
      link: "/tools/glassblower",
    },
    {
      title: "Healer's Kit",
      text: 'Caring for the wounded and the sick, healers embody love and compassion and often expand the lives of their companions.',
      link: "/tools/healer",
    },
    {
      title: 'Herbalism Kit',
      text: 'There is a herb for every occasion and master herbalists are always able to find the right one.',
      link: "/tools/herbalism",
    },
    {
      title: "Jeweler's Tools",
      text: 'Turning treasures into works of art and tools of magic, jewelers work with the most precious materials available.',
      link: "/tools/jeweler",
    },
    {
      title: "Leatherworker's Tools",
      text: 'Turning skin of slain creatures into tools and equipment, leatherworkers are always on the hunt for the next exotic skin.',
      link: "/tools/leatherworker",
    },
    {
      title: "Mason's Tools",
      text: 'Masters of stone, masons are able to both build and destroy creations of stone, slowly moving entire mountains.',
      link: "/tools/mason",
    },
    {
      title: "Navigator's Tools",
      text: 'Avoiding dangers and finding the shortest route, skilled navigator is able to safely navigate dangers of wilderness.',
      link: "/tools/navigator",
    },
    {
      title: "Painter's Supplies",
      text: 'Bringing pictures into life, sending messages and even stopping aging, painters become immortalized in their works of art.',
      link: "/tools/painter",
    },
    {
      title: "Poisoner's Kit",
      text: 'Shadowy art of the master poisoners is the subject of whispers and fear.',
      link: "/tools/poisoner",
    },
    {
      title: "Potter's Tools",
      text: 'Working with nothing but clay and fire, potters are able to create useful equipment and magical tools.',
      link: "/tools/potter",
    },
    {
      title: "Smith's Tools",
      text: 'Fixing armor, sharpening weapons, and improving tools of war, adventuring blacksmiths are welcome in any party.',
      link: "/tools/smith",
    },
    {
      title: "Thieves' Tools",
      text: 'Setting traps, getting past defences, master thieves are able to achieve their goals without ever showing themselves.',
      link: "/tools/thieves",
    },
    {
      title: "Tinker's Tools",
      text: 'Inventors and fixers, master tinkers use scraps to improve everything around them.',
      link: "/tools/tinker",
    },
    {
      title: "Weaver's Tools",
      text: 'Warm clothes in cold environments, sun shade in desert, weavers are conquering the wilderness one knot at the time.',
      link: "/tools/weaver",
    },
    {
      title: "Woodcarver's Tools",
      text: 'From religious carvings to the ornaments for the tilt of a battleaxe woodcarvers elevate their art to magical status.',
      link: "/tools/woodcarver",
    },
  ];

  constructor() {}

  ngOnInit(): void {}
}
