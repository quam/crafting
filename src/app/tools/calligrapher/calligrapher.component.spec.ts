import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CalligrapherComponent } from './calligrapher.component';

describe('CalligrapherComponent', () => {
  let component: CalligrapherComponent;
  let fixture: ComponentFixture<CalligrapherComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CalligrapherComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CalligrapherComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
