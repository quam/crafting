import { Component, OnInit } from '@angular/core';
import { RouterServiceService } from 'src/app/common/router-service.service';

@Component({
  selector: 'app-calligrapher',
  templateUrl: './calligrapher.component.html',
  styleUrls: ['./calligrapher.component.scss']
})
export class CalligrapherComponent implements OnInit {

  constructor(private router: RouterServiceService) {
    router.navigate();
  }

  ngOnInit(): void {
  }

}
