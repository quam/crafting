import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HerbalismComponent } from './herbalism.component';

describe('HerbalismComponent', () => {
  let component: HerbalismComponent;
  let fixture: ComponentFixture<HerbalismComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HerbalismComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HerbalismComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
