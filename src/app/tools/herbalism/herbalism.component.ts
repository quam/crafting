import { Component, OnInit } from '@angular/core';
import { RouterServiceService } from 'src/app/common/router-service.service';

@Component({
  selector: 'app-herbalism',
  templateUrl: './herbalism.component.html',
  styleUrls: ['./herbalism.component.scss']
})
export class HerbalismComponent implements OnInit {

  constructor(private router: RouterServiceService) {
    router.navigate();
  }

  ngOnInit(): void {
  }

}
