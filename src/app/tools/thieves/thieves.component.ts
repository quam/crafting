import { Component, OnInit } from '@angular/core';
import { RouterServiceService } from 'src/app/common/router-service.service';

@Component({
  selector: 'app-thieves',
  templateUrl: './thieves.component.html',
  styleUrls: ['./thieves.component.scss']
})
export class ThievesComponent implements OnInit {

  constructor(private router: RouterServiceService) {
    router.navigate();
  }

  ngOnInit(): void {
  }

}
