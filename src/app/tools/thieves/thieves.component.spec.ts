import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ThievesComponent } from './thieves.component';

describe('ThievesComponent', () => {
  let component: ThievesComponent;
  let fixture: ComponentFixture<ThievesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ThievesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ThievesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
