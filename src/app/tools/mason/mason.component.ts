import { Component, OnInit } from '@angular/core';
import { RouterServiceService } from 'src/app/common/router-service.service';

@Component({
  selector: 'app-mason',
  templateUrl: './mason.component.html',
  styleUrls: ['./mason.component.scss']
})
export class MasonComponent implements OnInit {

  constructor(private router: RouterServiceService) {
    router.navigate();
  }

  ngOnInit(): void {
  }

}
