import { Component, OnInit } from '@angular/core';
import { RouterServiceService } from 'src/app/common/router-service.service';

@Component({
  selector: 'app-cobbler',
  templateUrl: './cobbler.component.html',
  styleUrls: ['./cobbler.component.scss']
})
export class CobblerComponent implements OnInit {

  constructor(private router: RouterServiceService) {
    router.navigate();
  }

  ngOnInit(): void {
  }

}
