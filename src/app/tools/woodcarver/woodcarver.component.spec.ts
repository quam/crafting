import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WoodcarverComponent } from './woodcarver.component';

describe('WoodcarverComponent', () => {
  let component: WoodcarverComponent;
  let fixture: ComponentFixture<WoodcarverComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WoodcarverComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WoodcarverComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
