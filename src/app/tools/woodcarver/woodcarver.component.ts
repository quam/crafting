import { Component, OnInit } from '@angular/core';
import { RouterServiceService } from 'src/app/common/router-service.service';

@Component({
  selector: 'app-woodcarver',
  templateUrl: './woodcarver.component.html',
  styleUrls: ['./woodcarver.component.scss']
})
export class WoodcarverComponent implements OnInit {

  constructor(private router: RouterServiceService) {
    router.navigate();
  }

  ngOnInit(): void {
  }

}
