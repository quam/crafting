import { Component, OnInit } from '@angular/core';
import { RouterServiceService } from 'src/app/common/router-service.service';

@Component({
  selector: 'app-disguise',
  templateUrl: './disguise.component.html',
  styleUrls: ['./disguise.component.scss']
})
export class DisguiseComponent implements OnInit {

  constructor(private router: RouterServiceService) {
    router.navigate();
  }

  ngOnInit(): void {
  }

}
