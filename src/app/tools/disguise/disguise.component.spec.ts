import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DisguiseComponent } from './disguise.component';

describe('DisguiseComponent', () => {
  let component: DisguiseComponent;
  let fixture: ComponentFixture<DisguiseComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DisguiseComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DisguiseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
