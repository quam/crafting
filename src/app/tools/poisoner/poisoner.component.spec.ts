import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PoisonerComponent } from './poisoner.component';

describe('PoisonerComponent', () => {
  let component: PoisonerComponent;
  let fixture: ComponentFixture<PoisonerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PoisonerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PoisonerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
