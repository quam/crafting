import { Component, OnInit } from '@angular/core';
import { RouterServiceService } from 'src/app/common/router-service.service';

@Component({
  selector: 'app-poisoner',
  templateUrl: './poisoner.component.html',
  styleUrls: ['./poisoner.component.scss']
})
export class PoisonerComponent implements OnInit {

  constructor(private router: RouterServiceService) {
    router.navigate();
  }

  ngOnInit(): void {
  }

}
