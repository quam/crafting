import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-alc',
  templateUrl: './alc.component.html',
  styleUrls: ['./alc.component.scss']
})
export class AlcComponent implements OnInit {

  @Input()a: number = 0;
  @Input()c: number = 0;
  @Input()g: number = 0;
  @Input()p: number = 0;
  @Input()u: number = 0;

  constructor() { }

  ngOnInit(): void {
  }

}
