import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

//====================================CORE====================================//
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
//===================================COMMON===================================//
import { AlcComponent } from './common/alc/alc.component';
import { HeaderComponent } from './common/header/header.component';
import { HiddenLinkComponent } from './common/hidden-link/hidden-link.component';
import { LineMenuComponent } from './common/line-menu/line-menu.component';
import { MainComponent } from './common/main/main.component';
import { MenuComponent } from './common/menu/menu.component';
import { MonsterComponent } from './common/monster/monster.component';
import { SpellComponent } from './common/spell/spell.component';
//=================================MAIN MENU==================================//
import { MainMenuComponent } from './main-menu/main-menu.component';
//================================INTRODUCTION================================//
import { IntroductionComponent } from './introduction/introduction.component';
import { ComponentsComponent } from './introduction/components/components.component';
import { IntroForDmsComponent } from './introduction/intro-for-dms/intro-for-dms.component';
import { IntroForPlayersComponent } from './introduction/intro-for-players/intro-for-players.component';
//===================================TOOLS====================================//
import { ToolsComponent } from './tools/tools.component';
import { AlchemistComponent } from './tools/alchemist/alchemist.component';
import { BrewerComponent } from './tools/brewer/brewer.component';
import { CalligrapherComponent } from './tools/calligrapher/calligrapher.component';
import { CarpenterComponent } from './tools/carpenter/carpenter.component';
import { CartographerComponent } from './tools/cartographer/cartographer.component';
import { CobblerComponent } from './tools/cobbler/cobbler.component';
import { CookComponent } from './tools/cook/cook.component';
import { DisguiseComponent } from './tools/disguise/disguise.component';
import { ForgeryComponent } from './tools/forgery/forgery.component';
import { GlassblowerComponent } from './tools/glassblower/glassblower.component';
import { HealerComponent } from './tools/healer/healer.component';
import { HerbalismComponent } from './tools/herbalism/herbalism.component';
import { JewelerComponent } from './tools/jeweler/jeweler.component';
import { LeatherworkerComponent } from './tools/leatherworker/leatherworker.component';
import { MasonComponent } from './tools/mason/mason.component';
import { NavigatorComponent } from './tools/navigator/navigator.component';
import { PainterComponent } from './tools/painter/painter.component';
import { PoisonerComponent } from './tools/poisoner/poisoner.component';
import { PotterComponent } from './tools/potter/potter.component';
import { SmithComponent } from './tools/smith/smith.component';
import { ThievesComponent } from './tools/thieves/thieves.component';
import { TinkerComponent } from './tools/tinker/tinker.component';
import { WeaverComponent } from './tools/weaver/weaver.component';
import { WoodcarverComponent } from './tools/woodcarver/woodcarver.component';
//=================================REFERENCES=================================//
import { ReferencesComponent } from './references/references.component';
import { ExplosivesComponent } from './references/explosives/explosives.component';
import { PigmentsComponent } from './references/pigments/pigments.component';
import { PotionsComponent } from './references/potions/potions.component';
import { TincturesComponent } from './references/tinctures/tinctures.component';
//================================INGREDIENTS=================================//
import { IngredientsComponent } from './ingredients/ingredients.component';
import { IngredientComponent } from './ingredients/ingredient/ingredient.component';
import { AlphabeticalListComponent } from './ingredients/alphabetical-list/alphabetical-list.component';
import { AnimalsComponent } from './ingredients/animals/animals.component';
import { PlantsComponent } from './ingredients/plants/plants.component';
import { ResourcesComponent } from './ingredients/resources/resources.component';
import { ScavengingComponent } from './ingredients/scavenging/scavenging.component';
import { ItemComponent } from './common/item/item.component';

@NgModule({
  declarations: [
//====================================CORE=====================================//
    AppComponent,
//===================================COMMON====================================//
    AlcComponent,
    HeaderComponent,
    HiddenLinkComponent,
    LineMenuComponent,
    MainComponent,
    MenuComponent,
    MonsterComponent,
    SpellComponent,
//=================================MAIN MENU==================================//
    MainMenuComponent,
//================================INTRODUCTION================================//
    IntroductionComponent,
    ComponentsComponent,
    IntroForDmsComponent,
    IntroForPlayersComponent,
//===================================TOOLS====================================//
    ToolsComponent,
    AlchemistComponent,
    BrewerComponent,
    CalligrapherComponent,
    CarpenterComponent,
    CartographerComponent,
    CobblerComponent,
    CookComponent,
    DisguiseComponent,
    ForgeryComponent,
    GlassblowerComponent,
    HealerComponent,
    HerbalismComponent,
    JewelerComponent,
    LeatherworkerComponent,
    MasonComponent,
    NavigatorComponent,
    PainterComponent,
    PoisonerComponent,
    PotterComponent,
    SmithComponent,
    ThievesComponent,
    TinkerComponent,
    WeaverComponent,
    WoodcarverComponent,
//=================================REFERENCES=================================//
    ReferencesComponent,
    ExplosivesComponent,
    PigmentsComponent,
    PotionsComponent,
    TincturesComponent,
//================================INGREDIENTS=================================//
    IngredientsComponent,
    IngredientComponent,
    AlphabeticalListComponent,
    AnimalsComponent,
    PlantsComponent,
    ResourcesComponent,
    ScavengingComponent,
    ItemComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
